import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class QuestionText extends StatelessWidget {
  final String text;

  QuestionText(this.text);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.only(bottom: 30, top: 15),
      width: double.infinity,
      alignment: Alignment.center,
      child: Text(
        text,
        style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic),
      ),
    );
  }
}
