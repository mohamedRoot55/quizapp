import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BtnAnswer extends StatelessWidget {
  final Function answer_fn;

  //final int score ;
  final String answer_text;

  BtnAnswer({this.answer_fn, this.answer_text});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.only(top: 15, bottom: 15, left: 15, right: 15),
      width: double.infinity,
      //color: Colors.blue,
      child: RaisedButton(
        child: Text(
          answer_text,
          style: TextStyle(color: Colors.white),
        ),
        onPressed: answer_fn,
        color: Colors.blue,
      ),
    );
  }
}
