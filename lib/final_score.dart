import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class finalResult extends StatelessWidget {
  final String result;
  final Function reset;

  finalResult({this.result, this.reset});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 40),
          child: Column(
        children: <Widget>[
          Text(result),
          RaisedButton(

            child: Text("reset"),
            color: Colors.blue,
            onPressed: reset,
          ),
        ],
      )),
    );
  }
}
