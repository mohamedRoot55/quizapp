import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './TreeWidget.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  int Index_counter_rootList = 0;
  int totalScore = 0;

  var Questions = [
    {
      'question': 'what is your favourite color',
      'answer': [
        {'text': 'white', 'score': 1},
        {'text': 'green', 'score': 3},
        {'text': 'blue', 'score': 2},
        {'text': 'gray', 'score': 5}
      ]
    },
    {
      'question': 'what is your favourite animal',
      'answer': [
        {'text': 'dog', 'score': 1},
        {'text': 'rabbite', 'score': 3},
        {'text': 'lion', 'score': 2},
        {'text': 'qualaa', 'score': 5}
      ]
    },
    {
      'question': 'what is your favourite Opreating System',
      'answer': [
        {'text': 'mac', 'score': 1},
        {'text': 'windows', 'score': 3},
        {'text': 'linux', 'score': 2},
        {'text': 'android', 'score': 5}
      ]
    },
  ];

  void reset() {
    setState(() {
      if (Index_counter_rootList >= Questions.length) {
        Index_counter_rootList = 0;
        totalScore = 0;
      } else {
        null;
      }
    });
  }

  void _btn_answer_fn(int score) {
    totalScore += score;
    setState(() {
      Index_counter_rootList++;
    });
    print(Index_counter_rootList);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return root(
        counter: Index_counter_rootList,
        reset: reset,
        fn: _btn_answer_fn,
        Questions: Questions,
        totlascore: totalScore);
  }
}
//MaterialApp(
//home: Scaffold(
//appBar: AppBar(
//title: Text('Personal analysis'),
//centerTitle: true,
//elevation: 20.0,
//),
//body: Index_counter_rootList < Questions.length
//? Column(
//children: <Widget>[
//QuestionText(Questions[Index_counter_rootList]['question']),
//...(Questions[Index_counter_rootList]['answer']
//as List<Map<String, Object>>)
//.map((answer) {
//return BtnAnswer(
//answer_text: answer['text'],
//answer_fn: () => _btn_answer_fn(answer['score']),
//);
//}).toList()
//],
//)
//: finalResult(
//result: totalScore.toString(),
//reset: reset,
//),
//),
//)
