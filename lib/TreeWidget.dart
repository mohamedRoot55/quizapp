import 'package:flutter/material.dart';
import './Question_Text_Field.dart';
import './btn_answer.dart';
import './final_score.dart';

class root extends StatelessWidget {
  final int counter;
  final int totlascore;

  final Function fn;

  final Function reset;

  final List<Map<String, Object>> Questions;

  root({this.counter, this.Questions, this.fn, this.totlascore, this.reset});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Personal analysis'),
          centerTitle: true,
          elevation: 20.0,
        ),
        body: counter < Questions.length
            ? Column(
                children: <Widget>[
                  QuestionText(Questions[counter]['question']),
                  ...(Questions[counter]['answer'] as List<Map<String, Object>>)
                      .map((answer) {
                    return BtnAnswer(
                      answer_text: answer['text'],
                      answer_fn: () => fn(answer['score']),
                    );
                  }).toList()
                ],
              )
            : finalResult(
                result: totlascore.toString(),
                reset: reset,
              ),
      ),
    );
  }
}
